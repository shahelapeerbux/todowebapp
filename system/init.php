<?php
require_once('../vendor/autoload.php');

session_start();

//database configurations
$GLOBALS['Config'] = array(
           'mysql' => array(
                'host'=> 'localhost',
                'username'=>'shahela',
                'password'=>'shahela',
                'dbname'=> 'PHPTest'
           ),
        'session' => array(
            'session_name'=> 'user',
            'token_name' => 'token'
         )
);

//constants
define("ADMIN",1);
define("USER",0);
define("BASEURL", "http://localhost/ToDoWebApp/public");

spl_autoload_register(function($className){
    include "MVCFramework/$className.php";
});

$rout = new router();
