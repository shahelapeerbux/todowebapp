<?php


class database
{
    private static $instance = null;
    private $myPdo;
    private $myQuery;
    private $error = false;
    private $errorMsg = '';
    private $results;
    private $count = 0;

    public function __construct()
    {
        try {
            $connectionString = 'mysql:host=' . config::get('mysql/host') . ';dbname=' . config::get('mysql/dbname');
            $username = config::get('mysql/username');
            $password = config::get('mysql/password');
            $this->myPdo = new PDO($connectionString, $username, $password, [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]);
            //echo 'Connected';
        } catch (PDOException $e) {
            echo ($e->getMessage());
        }
    }

    public static function getInstance()
    {
        if (!isset(self::$instance)) {
            self::$instance = new database();
        }
        return self::$instance;
    }

    public function Query($qry, $params = [])
    {
        try {
            if (empty($params)) {
                $this->myQuery = $this->myPdo->prepare($qry);
                return $this->myQuery->execute();
            } else {
                $this->myQuery = $this->myPdo->prepare($qry);
                return $this->myQuery->execute($params);
            }
        } catch (PDOException $ex) {
            $this->errorMsg = $ex->getMessage();
            $this->error = true;
            die ($ex->getMessage());
        }
    }

    public function executeQuery($sql, $params = array(), $queryAction  = '')
    {
        try {
            $this->error = false;
            if ($this->myQuery = $this->myPdo->prepare($sql)) ;
            {
               $x = 1;
                if (count($params)) {
                    foreach ($params as $param) {
                        if (is_int($param)) {
                            $paramType = PDO::PARAM_INT;
                        } else if (is_bool($param)) {
                            $paramType = PDO::PARAM_BOOL;
                        } else if (is_null($param)) {
                            $paramType = PDO::PARAM_NULL;
                        } else if (is_string($param)) {
                            $paramType = PDO::PARAM_STR;
                        } else {
                            $paramType = FALSE;
                        }
                        $this->myQuery->bindValue($x, $param, $paramType);
                        $x++;
                    }
                }
                if ($this->myQuery->execute($params)) {
                    if ($queryAction == '') {
                        $this->results = $this->myQuery->fetchAll(PDO::FETCH_OBJ);
                        $this->count = $this->myQuery->rowCount();
                    }
                } else {
                    $this->errorMsg = -$this->myQuery->errorinfo();
                    $this->error = true;
                }
            }
        } catch (PDOException $ex) {
            $this->errorMsg = $ex->getMessage();
            $this->error = true;
            die ($ex->getMessage());
        }
    }

    private function action($action, $table, $where = array())
    {
        if (count($where) == 3) {
            $operators = array('=', '>', '<', '>=', '<=');
            $field = $where[0];
            $operator = $where[1];
            $value = $where[2];

            if (in_array($operator, $operators)) {
                $sql = "{$action} FROM {$table} WHERE {$field} {$operator} ?";

                if ($this->executeQuery($sql, array($value))->error()) {
                    return $this;
                }
            }
        }
        return false;
    }

    public function get($table, $where)
    {
        return $this->action('SELECT *', $table, $where);
    }

    public function delete($table, $where)
    {
        return $this->action('DELETE', $table, $where);
    }

    public function update($table, $id, $fields)
    {
        $set = '';
        $x = 1;

        foreach ($fields as $name => $value) {
            $set .= "{$name} = ?";
            if ($x < count($fields)) {
                $set .= ', ';
            }
            $x++;
        }
        $sql = "UPDATE {$table} SET {$set} WHERE ID = {$id}";
        echo $sql;

        if (!$this->executeQuery($sql, $fields)->error()) {
            return true;
        }
    }

    public function insert($table, $fields = array())
    {
        if (count($fields)) {
            $keys = array_keys($fields);
            $sql = "INSERT INTO {$table} (".implode(', ', $keys).") VALUES (".implode(',', array_fill(0, count($fields), '?')).")";

            if ($this->Query($sql, array_values($fields))) {
                return true;
            }
        }
        return false;
    }

    public function getResults()
    {
        if (!empty($this->results)) {
            return $this->results;
        } else {
            return [];
        }
    }

    public function count()
    {
        return $this->count;
    }

    public function error()
    {
        return $this->error;
    }

}
