<?php


class framework

{

    public function view($viewName, $data = [])
    {
        if (file_exists("../app/views/" . $viewName . ".php")) {
            require_once "../app/views/$viewName.php";
            //print $this->render("app/views/$viewName.php");
        } else {
            echo "View " . $viewName . " not found ";
        }
    }

    public  function model($modelName)
    {
        if (file_exists("../app/models/" . $modelName . ".php")) {
            require_once "../app/models/$modelName.php";
            $modelName= 'app\\models\\'. $modelName;
            return new $modelName;
        } else {
            echo "Model" . $modelName . " not found ";
        }
    }

    public function input($inputName)
    {
        if ($_SERVER['REQUEST_METHOD'] == "POST" || $_SERVER['REQUEST_METHOD'] == 'post') {
            return trim(filter_var($_POST[$inputName], FILTER_SANITIZE_STRING));

        } else if ($_SERVER['REQUEST_METHOD'] == 'GET' || $_SERVER['REQUEST_METHOD'] == 'get') {
            return trim(strip_tags($_GET[$inputName]));
        }
    }

    public static function sessionExists($tokenName)
    {
        return (isset($_SESSION[$tokenName])) ? true : false;
    }

    // Set session
    public function setSession($sessionName, $sessionValue)
    {
        if (!empty($sessionName) && !empty($sessionValue)) {
            $_SESSION[$sessionName] = $sessionValue;
        }
    }

    // Get session
    public function getSession($sessionName)
    {
        if(self::sessionExists($sessionName)) {
            return $_SESSION[$sessionName];
        }
    }

    // Unset session
    public function unsetSession($sessionName)
    {
        if (self::sessionExists($sessionName)) {
            unset($_SESSION[$sessionName]);
        }
    }

    // Destroy whole sessions
    public function destroy()
    {
        // Unset all of the session variables.
        $_SESSION = array();
        session_destroy();
    }

    // Set flash message
    public function setFlash($sessionName, $msg)
    {
        if (!empty($sessionName) && !empty($msg)) {
            $_SESSION[$sessionName] = $msg;
        }
    }

    //Show flash message
    public function flash($sessionName, $className)
    {
        if (self::sessionExists($sessionName)) {
            $msg = $_SESSION[$sessionName];
            echo "<div class='". $className ."'>".$msg."</div>";
            self::unsetSession($sessionName);
        }
    }

    public function redirect($path)
    {
        header("location:" . BASEURL . "/".$path);
    }


}