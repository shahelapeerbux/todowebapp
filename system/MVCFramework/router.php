<?php


class router
{
    //default controller and method
    public $controller = 'userController';
    public $method = 'index';
    public $params = [];

    public function __construct()
    {
        $url = $this->url();
        if (!empty($url)) {
            unset($url[0]);
            unset($url[1]);
            unset($url[2]);

            if (isset($url[3]) && (!empty($url[3]))) {
                if (file_exists("../app/controllers/" . $url[3] . ".php")) {
                    $this->controller = $url[3];
                    unset($url[3]);
                }
                else {
                  echo "Sorry ". $url[3] .".php not found";
                 }
            }
        }
        //require_once "../app/controllers/" .$this->controller . ".php";
        $this->controller = 'app\\controllers\\'. $this->controller;
        $this->controller = new $this->controller;

        if (isset($url[4]) && (!empty($url[4]))) {
            if (method_exists($this->controller,$url[4])) {
                $this->method = $url[4];
                unset($url[4]);
            } else {
                echo "Sorry ". $url[4] ." method not found";
            }
        }

        if (isset($url)) {
            $this->params = $url;
        } else {
            $this->params = [];
        }
        call_user_func_array([$this->controller, $this->method], $this->params);
    }

    public function url()
    {
        if (isset($_GET['url'])) {
            $url = $_GET['url'];
            $url = rtrim($url);
            $url = filter_var($url, FILTER_SANITIZE_URL);
            $url = explode("/", $url);
            return $url;
        }
    }
}