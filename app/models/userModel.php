<?php

namespace app\models;

class userModel extends \database

{
    // object properties
    public $userId;
    public $firstName;
    public $lastName;
    public $email;
    public $password;
    public $accessLevel;

    public function checkUserLogin($email, $inputPassword)
    {
        $this->executeQuery("SELECT * FROM Users WHERE email = ? ", [$email]);
            if ($this->count() > 0 ) {
                $result= $this->getResults();
                $this->userId = $result[0]->id;
                $this->accessLevel = $result[0]->role_id;
                $this->firstName = $result[0]->firstname;
                $this->lastName = $result[0]->lastname;
                $this->password = $result[0]->password;
                $this->email = $result[0]->email;

                if (password_verify($inputPassword, $this->password)) {
                   return ['status' => 'ok'];
                } else {
                    return ['status' => 'passwordNotMatched'];
                }
            } else {
                return ['status' => 'emailNotFound'];
            }
        }

    public function emailExists($email)
    {
        $this->executeQuery("SELECT email FROM Users WHERE email = ?", [$email]);
        if ($this->count() > 0 ) {
            return true;
        } else {
            return false;
        }
    }

    public function createAccount($data)
    {
       if ($this->insert('Users', $data)) {
            return true;
        }
        /* if($this->Query("INSERT INTO Users (firstname, lastname, password, role_id, email) VALUES (?,?,?,?,?)", array_values($data))){
            return true;
        }*/
    }

}