<?php

namespace app\models;

class profileModel extends \database

{

    public function addTask ($tasks)
    {
        if ($this->insert('Tasks', $tasks)) {
            return true;
        }
    }

    public function getTaskLists ($userId)
    {
        $this->executeQuery("SELECT task_id,title, status FROM Tasks WHERE user_id = ?",[$userId]);
        if ($this->count() > 0 ) {
            return $this->getResults();
        }
    }

    public function UpdateTask ($userId, $taskId,$status)
    {
        if($this->Query("UPDATE Tasks SET status = ? WHERE task_id = ?  AND user_id = ? ", [$status,$taskId, $userId])){
            return true;
        }
    }

    public function deleteTask ($userId, $taskId)
    {
        if($this->Query("DELETE FROM Tasks WHERE task_id = ? && user_id = ? ", [$taskId, $userId])){
              return true;
          }
    }

    public function searchTask ($userId, $taskName)
    {
        $this->executeQuery("SELECT task_id,title, status FROM Tasks WHERE title like ? AND user_id = ?",["%$taskName%",$userId]);
        if ($this->count() > 0 ) {
            return $this->getResults();
        }
    }

}