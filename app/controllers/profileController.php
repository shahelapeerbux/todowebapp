<?php

namespace app\controllers;

class profileController extends \framework
{

    public function __construct()
    {
        if (!$this->isLoggedIn() ) {
            $this->redirect("userController/loginForm");
        }
        $this->profileModel = $this->model("profileModel");
    }

    public function index()
    {
        $userId = $this->getSession('userId');
        $data = $this->profileModel->getTaskLists($userId);

        $this->view("userView", $data);
    }

    public function isLoggedIn()
    {
        if ($this->getSession('userId')) {
            return true;
        }
    }

    public function isAdmin()
    {
        if ($this->isLoggedIn() && $this->getSession('access_level') == ADMIN ) {
            return true;
        }
    }

    public function logout()
    {
        $this->destroy();
        $this->redirect("userController/loginForm");
    }

    public function validateAndAddNewTask()
    {
        $taskName = htmlspecialchars($this->input('addTask'), ENT_QUOTES, 'UTF-8');
        if ($this->addNewTask($taskName)) {
            $this->setFlash("taskAdded", "Your task has been added successfully!");
            $this->redirect("profileController/index");
        } else {
            $taskData['Error'] = "Error adding task!";
            $this->view("userView");
        }
    }

    public function addNewTask($taskName)
    {
        if (!empty($taskName)) {
            $today = date("Y-m-d H:i:s");
            $tasks = ['title' => $taskName,
                      'description' => $taskName,
                      'user_id' => $this->getSESSION('userId'),
                      'status'=> 0,
                      'datecreated' => $today
            ];
           return $this->profileModel->addTask($tasks);
        }
    }

    public function searchTask()
    {
        $searchItem = $this->input('searchTask');
        if (!empty($searchItem)) {
            $searchData = $this->profileModel->searchTask($this->getSESSION('userId'), $searchItem);
            if (!empty($searchData)) {
                //search successful
                $this->setFlash('taskFound', 'Search results below for "' . $searchItem . '""...');
                $this->view("userView", $searchData);
            } else {
                //reload all tasks
                $this->setFlash('taskNotFound', 'Your search "' . $searchItem . '"" did not match any tasks!');
                $this->redirect("profileController/index");
            }
        }
    }

    public function doneTask($taskId)
    {
        $userId = $this->getSESSION('userId');
        if ($this->profileModel->UpdateTask($userId, $taskId, 1)) {
            $this->redirect("profileController/index");
        }
    }

    public function unDoTask($taskId)
    {
        $userId = $this->getSESSION('userId');
        if ($this->profileModel->UpdateTask($userId, $taskId, 0)) {
            $this->redirect("profileController/index");
        }
    }

    public function deleteTask($taskId)
    {
        $userId = $this->getSESSION('userId');
        if ($this->profileModel->deleteTask($userId, $taskId)) {
            $this->setFlash("taskDeleted", "Your task has been deleted successfully!");
            $this->redirect("profileController/index");
        }
     }

}