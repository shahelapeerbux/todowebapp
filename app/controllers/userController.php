<?php

namespace app\controllers;

class userController extends \framework

{
    public function __construct()
    {
        if ($this->getSession('userId')) {
            $this->redirect("profileController");
         }
           $this->userModel = $this->model('userModel');
           //$this->loginForm();
    }

    public function index()
    {
        // $this->view("test");
        $this->loginForm();
        //defaults to register
        //$this->view("register");
    }

    public function loginForm()
    {
        $this->view("login");
    }

    public function userLogin()
    {
        $userData = [
            'email'         => filter_var($this->input('email'),FILTER_SANITIZE_EMAIL),
            'password'      => $this->input('password'),
            'emailError'    => '',
            'passwordError' => ''
        ];
        $valid = true;

        if (empty($userData['email'])) {
            $userData['emailError'] = "Email is required";
            $valid = false;
        }

        if (empty($userData['password'])) {
            $userData['passwordError'] = "Password is required";
            $valid = false;
        }

        if ($valid) {
            $result = $this->userModel->checkUserLogin($userData['email'], $userData['password']);

            switch ($result['status']) {
                case 'emailNotFound':
                    $userData['emailError'] = "Sorry invalid email";
                    $this->view("login", $userData);
                    break;
                case 'passwordNotMatched':
                    //$userData['passwordError'] = "Sorry invalid password";
                    $this->setFlash("validationErrors", "Sorry invalid password");
                    $this->view("login", $userData);
                    break;
                case 'ok':
                    $this->setSession("userId", $this->userModel->userId);
                    $this->setSession("access_level", $this->userModel->accessLevel);
                    $this->setSession("firstName",  $this->userModel->firstName);

                    $this->redirect("profile");
                    break;
            }
        } else {
            $this->view("login", $userData);
        }
    }

    public function createAccount()
    {
        //role 1 = user
        //role 2 = admin
        //defaulting all users to non-admin
        $userData = [
            'email'           => filter_var($this->input('email'),FILTER_SANITIZE_EMAIL),
            'firstName'       => $this->input('firstname'),
            'lastName'        => $this->input('lastname'),
            'password'        => $this->input('password'),
            'confirmPassword' => $this->input('confirm-password'),
            'role_id'         => USER,
            'FNameError'      => '',
            'LNameError'      => '',
            'emailError'      => '',
            'passwordError'   => '',
            'confirmPwdError' => ''
        ];

        if (empty($userData['firstName'])) {
            $userData['FNameError'] = 'First Name is required';
        }
        if (empty($userData['lastName'])) {
            $userData['LNameError'] = 'Last Name is required';
        }
        if (empty($userData['email'])) {
            $userData['emailError'] = 'Email is required';
        } else {
            if ($this->userModel->emailExists($userData['email'])) {
                $userData['emailError'] = "Sorry this email is already taken!";
            }
        }

        $userData['passwordError'] = $this->validatePassword($userData['password']);

        if (!empty($userData['passwordError'])) {
            if (empty($confirmPassword)) {
                $userData['confirmPwdError'] = "Please confirm password again...";
            } else if ($userData['password']!= $userData['confirmPassword']) {
                $userData['confirmPwdError'] = "Password does not match";
            }
        }

        if (empty($userData['FNameError']) && empty($userData['emailError']) && empty($userData['passwordError']) && empty($userData['confirmPwdError'])) {
            $password = password_hash($userData['password'], PASSWORD_DEFAULT);
            $data = ['firstname' => $userData['firstName'],
                'lastname' =>  $userData['lastName'],
                'password' => $password,
                'role_id'=> $userData['role_id'],
                'email' =>  $userData['email']
            ];

            if ($this->userModel->createAccount($data)) {
                $this->setFlash("accountCreated", "Your account has been created successfully");
                $this->redirect("userController/loginForm");
            }
        } else {
            $this->view('register', $userData);
        }
    }

    private function validatePassword($password)
    {
        $errorMessage = '';
        if (empty($password)) {
            $errorMessage = "Password is required";
        }
        else if(preg_match("/^.*(?=.{5,})(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z]).*$/", $password) == 0) {
            $errorMessage .= "Password must be at least 5 characters and must contain at least one lower case letter, one upper case letter and one digit";
        }
        return $errorMessage;
    }

}