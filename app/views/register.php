<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>User Registration</title>
    <link rel="stylesheet" href="<?php echo BASEURL; ?>/assets/stylesheet.css">
    <style>
        .success {
            padding: 20px;
            background-color: darkolivegreen;
            color: khaki;
        }
    </style>
</head>
<body>
<div class="box">
    <h2>Register User</h2>
    <form id="register-form"  method="post" action="<?php echo BASEURL; ?>/userController/createAccount">
        <div class="inputBox">
            <input type="text" name="email" required="" autocomplete="off" value="<?php if(!empty($data['email'])): echo $data['email']; endif; ?>">
            <label>Email</label>
            <div class="error">
                <?php if(!empty($data['emailError'])): echo $data['emailError']; endif; ?>
            </div>
        </div>
        <div class="inputBox">
            <input type="text" name="firstname" required="" autocomplete="off">
            <label>First Name</label>
            <div class="error">
                <?php if(!empty($data['FNameError'])): echo $data['FNameError']; endif; ?>
            </div>
        </div>
        <div class="inputBox">
            <input type="text" name="lastname" required="" autocomplete="off">
            <label>Last Name</label>
            <div class="error">
                <?php if(!empty($data['LNameError'])): echo $data['LNameError']; endif; ?>
            </div>
        </div>
        <div class="inputBox">
            <input type="password" name="password" required="" autocomplete="off">
            <label>Password</label>
            <div class="error">
                <?php if(!empty($data['passwordError'])): echo $data['passwordError']; endif; ?>
            </div>
        </div>
        <div class="inputBox">
            <input type="password" name="confirm-password" required="" autocomplete="off">
            <label>Confirm Password</label>
            <div class="error">
                <?php if(!empty($data['confirmPwdError'])): echo $data['confirmPwdError']; endif; ?>
            </div>
        </div>
        <input type="submit" name="register-submit"  id="register-submit"  value="Save">
    </form>

</div>


</body>
</html>