<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>To do list</title>
    <link rel="stylesheet" href="<?php echo BASEURL; ?>/assets/stylesheet.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style>
        .success {
            padding: 20px;
            background-color: darkolivegreen;
            color: khaki;
        }
        .alert-error {
            padding: 20px;
            background-color: #f44336;
            color: white;
        }
    </style>
</head>
<body>

<?php include "inc/header.php"; ?>
<?php include "inc/messages.php"; ?>

<div class="list">
    <img width="100" height="80" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTXHuBPj6qab-uPEuzD_qNa8qBA2gae5DTqbr14szcV5VP3Y1rs&s">

    <?php if(!empty($data)): ?>
        <ul class="items">
            <?php foreach($data as $items):
                $item = get_object_vars($items);
            ?>
                <li>
                    <span class="item<?php echo $item['status'] ? ' done' : ''?>"> <?php echo $item['title']; ?></span>
                    <?php if ($this->getSession('access_level') == ADMIN) {
                            include "inc/admin.php";
                          } ?>
                </li>
            <?php endforeach; ?>
        </ul>
    <?php else: ?>
        <p>You do not have any items yet!</p>
    <?php endif; ?>

    <form class="item-add" action="<?php echo BASEURL;?>/profileController/validateAndAddNewTask" method="POST">
        <input type="text" name="addTask" placeholder="Type a new to-do item here.." class="input" autocomplete="off" maxlength="30" required>
        <input type="submit" value="Add" class="submit">
    </form>

    <form class="item-add" action="<?php echo BASEURL;?>/profileController/searchTask"  method="POST">
        <input type="text" name="searchTask" placeholder="Search for to-do items here.." class="input" autocomplete="off" maxlength="30" required>
        <a class="done-button" href="<?php echo BASEURL; ?>/profileController/index">Clear Search</a>
        <input type="submit" value="Search" class="submit">
    </form>

</div>

</body>

