<a class="delete-button" title="Delete Task" href="<?php echo BASEURL; ?>/profileController/deleteTask/<?php echo $item['task_id']; ?>">X</a>
<?php if(!$item['status']): ?>
    <a class="done-button" title="Mark as done" href="<?php echo BASEURL; ?>/profileController/doneTask/<?php echo $item['task_id']; ?>">&#10003; Done</a>
<?php else: ?>
    <a class="undone-button" title="Mark as Undone" href="<?php echo BASEURL; ?>/profileController/unDoTask/<?php echo $item['task_id']; ?>">&#10003; UnDo</a>
<?php endif; ?>