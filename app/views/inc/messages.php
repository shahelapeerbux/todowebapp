<?php
$this->flash('taskAdded', 'success');
$this->flash('taskFound', 'success');
$this->flash('taskDeleted', 'success');
$this->flash('taskNotFound', 'alert-error');
$this->flash('validationErrors', 'alert-error');
$this->flash('accountCreated', 'success');
