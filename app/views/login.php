<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Login </title>
    <link rel="stylesheet" href="<?php echo BASEURL; ?>/assets/stylesheet.css">
    <style>
        .success {
            padding: 20px;
            background-color: darkolivegreen;
            color: khaki;
        }
    </style>

</head>
<body>

<?php include "inc/header.php"; ?>
<?php include "inc/messages.php"; ?>

<div class="box">
    <h2>Login</h2>
    <form method="post" action="<?php echo BASEURL; ?>/userController/userLogin">
        <div class="inputBox">
            <input type="text" name="email" required="" value="<?php if(!empty($data['email'])): echo $data['email']; endif; ?>">
            <label>Email</label>
        </div>
        <div class="error">
            <?php if(!empty($data['emailError'])): echo $data['emailError']; endif; ?>
        </div>
        <div class="inputBox">
            <input type="password" name="password" required=""  value="<?php if(!empty($data['password'])): echo $data['password']; endif; ?>">
            <label>Password</label>
        </div>
        <div class="error">
            <?php if(!empty($data['passwordError'])): echo $data['passwordError']; endif; ?>
        </div>
        <input type="submit" name="" id="submit" value="submit">
    </form>
</div>


</body>

</html>