<?php

class profileControllerTest extends \PHPUnit\Framework\TestCase
{
    public function testAddNewTask()
    {
        $profileController = new \app\controllers\profileController;

        //test data start
        $inputTask = 'This is a unit test task';
        $sessionUserId = 2;
        //test data end

        $inputTaskMock =$this->createMock(app\controllers\profileController::class);
        //mock the user session
        $inputTaskMock->expects($this->once())->method('getSESSION')->will($this->returnvalue($sessionUserId));
        $this->assertTrue($inputTask,$profileController->addNewTask());
    }

}