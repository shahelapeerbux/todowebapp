-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Nov 11, 2019 at 06:13 AM
-- Server version: 5.7.27-0ubuntu0.18.04.1
-- PHP Version: 7.1.32-1+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `PHPTest`
--

-- --------------------------------------------------------

--
-- Table structure for table `Roles`
--

CREATE TABLE `Roles` (
  `role_id` int(11) NOT NULL,
  `rolename` varchar(50) NOT NULL,
  `permission` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Roles`
--

INSERT INTO `Roles` (`role_id`, `rolename`, `permission`) VALUES
(1, 'admin', 1),
(2, 'user', 0);

-- --------------------------------------------------------

--
-- Table structure for table `Tasks`
--

CREATE TABLE `Tasks` (
  `task_id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `description` varchar(200) NOT NULL,
  `status` tinyint(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `datecreated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Tasks`
--

INSERT INTO `Tasks` (`task_id`, `title`, `description`, `status`, `user_id`, `datecreated`) VALUES
(2, 'test 2 done', 'Test 2 done', 1, 1, '2019-10-29 20:00:00'),
(3, 'db insert', '', 0, 1, '2019-10-29 20:00:00'),
(4, 'test db2', 'test db2', 1, 1, '2019-10-29 20:00:00'),
(6, 'test web2', 'test web2', 0, 1, '2019-10-29 20:00:00'),
(7, 'test web 3', 'test web 3', 0, 17, '2019-11-06 14:30:43'),
(8, 'pay the bills', 'pay the bills', 0, 17, '2019-11-06 14:01:20'),
(13, 'call my princess', 'call my princess', 0, 17, '2019-11-06 10:31:03'),
(14, 'test for admin task', 'test for admin task', 0, 13, '2019-11-08 08:12:45');

-- --------------------------------------------------------

--
-- Table structure for table `Users`
--

CREATE TABLE `Users` (
  `id` int(11) NOT NULL,
  `firstname` varchar(100) NOT NULL,
  `lastname` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `role_id` int(11) NOT NULL,
  `email` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Users`
--

INSERT INTO `Users` (`id`, `firstname`, `lastname`, `password`, `role_id`, `email`) VALUES
(1, 'Shahela', 'Peerbux', 'test', 1, 'shahela2@gmail.com'),
(13, 'Shahela', 'Peerbux', '$2y$10$mNJ.VerxY8hxFxvV5xJdaeyVzVYvB2vWX9UE3nf8OTdW0WpEgq6ka', 2, 'shahela@gmail.com'),
(14, 'testfname', 'testname', '$2y$10$s5OGDzgxLfNVkhPbTSXaauIauRqVKXMnJcBKIq1BVnYoRczO98KKW', 1, 'test@gmail.com'),
(15, 'choupi', 'choups', '$2y$10$fBp60HG6yec6qlUjk9C9keygUVw9.VJ2dIYkdsOFKG0yPPlgRqTzi', 1, 'choupi@gmail.com'),
(17, 'Shahela', 'Peerbux', '$2y$10$fA7Eyj.ahV4qSJBLtp4JMO9zua8kK7gIycVHDB7M/bWDSPaabBTGa', 1, 'shahela3@gmail.com');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Roles`
--
ALTER TABLE `Roles`
  ADD PRIMARY KEY (`role_id`);

--
-- Indexes for table `Tasks`
--
ALTER TABLE `Tasks`
  ADD PRIMARY KEY (`task_id`);

--
-- Indexes for table `Users`
--
ALTER TABLE `Users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Roles`
--
ALTER TABLE `Roles`
  MODIFY `role_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `Tasks`
--
ALTER TABLE `Tasks`
  MODIFY `task_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `Users`
--
ALTER TABLE `Users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
